import numpy as np

class AKernel:
	def __init__(self,kernelFunction,representationUsed,normalize=True):
		self.kernelMemory = {}
		self.kernelFunction = kernelFunction
		self.representationUsed = representationUsed
		self.normalize = normalize

	#normalise kernel by k(x,y) = k(x,y)/ (sqrt(k(x,x)*k(y,y)))
	def normalise(self,sample1,sample2,kernelValue12):
		k11 = self.useKernelUnNormalized(sample1,sample1)
		k22 = self.useKernelUnNormalized(sample2,sample2)

		if k11 == 0 or k22 ==0:
			return 0
		return (kernelValue12 / (np.sqrt(k11 * k22)))


	def getFeatureVector(self,sample):
		return sample.representation.getRepresentation(self.representationUsed)

	def useKernel(self,sample1,sample2):
		if self.normalize:
			return self.useKernelNormalized(sample1,sample2)
		else:
			return self.useKernelUnNormalized(sample1,sample2)
	
	def useKernelNormalized(self,sample1,sample2):
		unNormalisedKernelValue = self.useKernelUnNormalized(sample1,sample2)
		return self.normalise(sample1,sample2,unNormalisedKernelValue)#normalise if need be

	def useKernelUnNormalized(self,sample1,sample2):
		rememberKernelValue = self.getFromMemory(sample1,sample2)
		KernelValueToReturn = 0
		if (rememberKernelValue):
			KernelValueToReturn = rememberKernelValue
		else:
			sample1FeatureVector = self.getFeatureVector(sample1)
			sample2FeatureVector = self.getFeatureVector(sample2)

			KernelValueToReturn = self.kernelFunction(sample1FeatureVector,sample2FeatureVector)
			self.saveInMemory(sample1,sample2,KernelValueToReturn)#save in memory (unnormalised) for later use
		
		return KernelValueToReturn


	def getKeyForMemory(self,sample1,sample2):
		return (sample1.getID(),sample2.getID())##fix me!
	def saveInMemory(self,sample1,sample2,value):
		self.kernelMemory[self.getKeyForMemory(sample1,sample2)] = value
	def getFromMemory(self,sample1,sample2):
		possibleKey1 = self.getKeyForMemory(sample1,sample2)
		if possibleKey1 in self.kernelMemory:
			return self.kernelMemory[possibleKey1]
		
		possibleKey2 = self.getKeyForMemory(sample2,sample1)
		if possibleKey2 in self.kernelMemory:
			return self.kernelMemory[possibleKey2]

		return False;#Kernel value not in memory return false



def BOWcosine(rep1,rep2):
	return cosineSim(rep1.getBOW(),rep2.getBOW())
def BOEmbeddingscosine(rep1,rep2):
	res = cosineSim(rep1.getBOEmbeddings(), rep2.getBOEmbeddings())
	return res
#cosine similarity
def dotProduct(vector1,vector2):
	return np.dot(vector1,vector2)

def cosineSim(vector1,vector2):
	numer = np.vdot(vector1,vector2);
	denom = np.linalg.norm(vector1) * np.linalg.norm(vector2)
	if denom == 0 or numer ==0:
		return 0;
	return numer/denom
#Given two bag of words 
def countSimilarFeatures(vector1,vector2):
	simScore = 0
	vector1Size = 0;
	vector2Size = 0;
	for i in range(len(vector1)):
		v1Has = vector1[i] > 0
		v2Has = vector2[i] > 0
		if v1Has:
			vector1Size = vector1Size + 1
		if v2Has:
			vector2Size = vector2Size + 1
		if v1Has and v2Has > 0:
			simScore = simScore + 1

	if(vector1Size == 0 or vector2Size == 0):
		return 0
	return simScore/(np.sqrt(vector1Size) * np.sqrt(vector2Size) )


class QAKernel:
	def __init__(self,questionKernels,answerKernels,questionKernelWeight,answerKernelWeight):
		self.questionKernels = questionKernels
		self.answerKernels = answerKernels
		self.questionKernelWeight = questionKernelWeight
		self.answerKernelWeight = answerKernelWeight
	def getTrainingGramMatrix(self,QAPairs):
		numberOfSamples = len(QAPairs)
		gramMat = np.empty([numberOfSamples,numberOfSamples], dtype=float)

		for x in range(numberOfSamples):
			#print x+1 , 'out of' , numberOfSamples
			for y in range(x,numberOfSamples):
				combinedKernelResult = self.computeQAPairKenerl(QAPairs[x],QAPairs[y])
				gramMat[x,y] = combinedKernelResult
				gramMat[y,x] = combinedKernelResult
		
		return gramMat

	def getTestingGramMatrix(self,QAPairsSupport,QAPairsTesting):
		numberOfSamples = len(QAPairsSupport)
		numberOfTesting = len(QAPairsTesting)
		gramMat = np.empty([numberOfTesting,numberOfSamples], dtype=float)

		for x in range(numberOfSamples):
			for y in range(numberOfTesting):
				combinedKernelResult = self.computeQAPairKenerl(QAPairsSupport[x],QAPairsTesting[y])
				gramMat[y,x] = combinedKernelResult

		return gramMat

	def computeQAPairKenerl(self,(questionVector1,answerVector1),(questionVector2,answerVector2)):
		questionKernelSum = self.useQuestionKernels(questionVector1,questionVector2)
		answerKernelSum = self.useAnswerKernels(answerVector1,answerVector2)
		
		combinedQAKernelResult = self.combineQuestionAnswerKernels(questionKernelSum,answerKernelSum)

		return combinedQAKernelResult
	def combineQuestionAnswerKernels(self,questionKernelSum,answerKernelSum):
		return (self.questionKernelWeight * questionKernelSum) + (self.answerKernelWeight * answerKernelSum)

	def useQuestionKernels(self,sample1,sample2):
		return useKernels(sample1,sample2,self.questionKernels)

	def useAnswerKernels(self,sample1,sample2):
		return useKernels(sample1,sample2,self.answerKernels)


def useKernels(sample1,sample2,kernels):
	total = 0;
	for (questionKernel,kernelWeight) in kernels:
		kernelValue = questionKernel.useKernel(sample1,sample2)
		total = total + (kernelValue * kernelWeight)
	return total