
import kernels
import representations
from kernels import AKernel

import treeKernelNormal
import treeEmbeddingMod



def createBOWCosineKernel():
	return AKernel(kernels.cosineSim,representations.BOW,False)


def createSyntacticParseTreeKernel():
	return AKernel(treeKernelNormal.tk,representations.SYNTACTICTREE,True)

def createBOEmbeddingsKernel():
	return AKernel(kernels.cosineSim,representations.BOWEmbeddingsAveraged,False)

def createSyntacticParseTreeKernelEmbeddingMod():
	return AKernel(treeEmbeddingMod.tkEmbedMod,representations.SYNTACTICTREE,True)
