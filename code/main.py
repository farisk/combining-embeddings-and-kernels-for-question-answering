import featureVectors
from answerExtractionSVM import AnswerExtractionSVM

from kernels import AKernel
from kernels import QAKernel
import kernelFactory
import dataLoader
import sys
reload(sys)  
sys.setdefaultencoding('utf8')#Needed when reading in parse-trees: otherwise error thrown


def splitQuestions(questions,numberOfGroups):
    groupedQuestions = []
    numberOfQuestions = len(questions)
    sizeOfGroup = numberOfQuestions/numberOfGroups#python rounds down

    currentGroup = []
    numberOfQuestionsEquallyDistributed = sizeOfGroup*numberOfGroups
    print numberOfQuestionsEquallyDistributed
    for i in range(numberOfQuestionsEquallyDistributed):
        currentGroup.append(questions[i])
        if( (i+1) % sizeOfGroup == 0):
            #Enough elements have been put onto group, add to list and start next one
            groupedQuestions.append(currentGroup)
            currentGroup = []
    #Distribute left over questions into each group until no more
    leftOverQuestions = numberOfQuestions-numberOfQuestionsEquallyDistributed
    startIndex = numberOfQuestions-leftOverQuestions;
    groupSelected = 0
    for i in range(startIndex,numberOfQuestions):
        groupedQuestions[groupSelected].append(questions[i])
        groupSelected = (groupSelected + 1) % numberOfGroups
    return groupedQuestions



def countNegPos(answerList):
    posina = 0
    negina = 0
    for a in answerList:
        if a.isPositive():
            posina = posina + 1
        else:
            negina = negina + 1
    return (posina,negina)

def combineListOfLists(groupList):
    return [q for group in groupList for q in group]

def getAllCombinations(groupList):
    combo = [];
    numberOfGroups = len(groupList)

    for i in range(numberOfGroups):
        testingQuestions = groupList[i]
        trainingQuestions = groupList[:]#deep copy group list
        trainingQuestions.remove(testingQuestions)#remove testing from training list
        trainingQuestions = combineListOfLists(trainingQuestions)#combine all training groups into one big group
        combo.append((trainingQuestions,testingQuestions))
    return combo


#Load question and answer data, process into class instances
whoQuestions = dataLoader.loadQuestions("data/whereQuestionsPID.txt")
answers = dataLoader.loadAnswersToQuestions(whoQuestions,"data/WhereAnswersIDClass.txt")

print "Number of questions:", len(whoQuestions)
(postiveSamples,negativeSamples) = countNegPos(answers)
print "Number of answers:" , len(answers) , ' Positive:' , postiveSamples , ' Negative:' , negativeSamples


#Form feature vectors
featureVectors.loadParseTrees(answers,"data/WhereParseTrees.parsed")#load answer parse trees

#BOW
featureVectors.samplesToBOW(whoQuestions.values())
featureVectors.samplesToBOW(answers)

featureVectors.samplesToBagOfWordEmbeddingMax(whoQuestions.values())
featureVectors.samplesToBagOfWordEmbeddingMax(answers)


#Define kernels
BOWKernelQuestion =kernelFactory.createBOWCosineKernel()
BOWKernelAnswer = kernelFactory.createBOWCosineKernel()

syntacticParseTreeKernelAnswer = kernelFactory.createSyntacticParseTreeKernel()
#syntacticParseTreeKernelQuestion = kernelFactory.createSyntacticParseTreeKernel()

syntacticParseTreeEmbeddingKernelAnswer = kernelFactory.createSyntacticParseTreeKernelEmbeddingMod()
#syntacticParseTreeEmbeddingKernelQuestion = kernelFactory.createSyntacticParseTreeKernelEmbeddingMod()

BOEKernelQuestion = kernelFactory.createBOEmbeddingsKernel()
BOEKernelAnswer= kernelFactory.createBOEmbeddingsKernel()

#Split Data into five grouos
splitQuestions = splitQuestions(whoQuestions.values(),5)
trainTestCombinations = getAllCombinations(splitQuestions)#Get different combinations of the five groups(1 test, 4 train)
(trainingQuestions,testingQuestions) = trainTestCombinations[len(trainTestCombinations)-1]




#Form answer extraction svm based on training groups and range of cost factors
binarySVM = AnswerExtractionSVM(trainTestCombinations,[0.2,0.4,0.6,0.8,1,1.2,1.4,1.6,1.8,2])

#Form question and answer kernel
questionKernel = [(BOWKernelQuestion,1)]

#answerKernel = [(syntacticParseTreeKernelAnswer,0.3),(BOEKernelQuestion,0.7)]#A(PT+BOE)
#answerKernel = [(syntacticParseTreeKernelAnswer,0.3),(BOEKernelQuestion,0.7)]#A(PT+BOE)
#answerKernel = [(syntacticParseTreeEmbeddingKernelAnswer,1)]#A(PT+BOE)
#answerKernel = [(syntacticParseTreeEmbeddingKernelAnswer,1)]#A(PT+BOE)
#answerKernel = [(BOEKernelAnswer,1)]#A(PT+BOE)
answerKernel = [(BOWKernelAnswer,1)]#A(PT+BOE)
qaKernel = QAKernel(questionKernel,answerKernel,0.5,0.5)
#Run SVM on the defined Kernel
binarySVM.useQAKernel(qaKernel)


