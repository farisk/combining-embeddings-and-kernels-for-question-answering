BOW = "BOW"
TFIDF = "TFIDF"
SYNTACTICTREE = "SYNTACTICTREE"
BOWEmbeddingsAveraged = "BOWEMBEDDINGSAVERAGED"
class Representations:

	
	def __init__(self):
		self.representationDict = {}


	def representationSet(self,rKey):
		return rKey in self.representationDict

	def getRepresentation(self,rKey):
		if self.representationSet(BOW):
			return self.representationDict[rKey]
		else:
			raise(rKey + " representation has not been set")

	def setRepresentation(self,rKey,value):
		self.representationDict[rKey] = value

	def getBOW(self):
		return self.getRepresentation(BOW)

	def setBOW(self,BOWVector):
		self.setRepresentation(BOW,BOWVector)

	def isBowSet(self):
		return self.representationSet(BOW)


	def getBOEmbeddings(self):
		return self.getRepresentation(BOWEmbeddingsAveraged)

	def getSyntacticParseTree(self):
		return self.getRepresentation(SYNTACTICTREE)


