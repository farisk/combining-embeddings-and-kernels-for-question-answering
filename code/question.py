from representations import Representations
class Question:
    

    def __init__(self,question,questionID):
        self.sentence = question
        self.representation = Representations()
        self.positiveAnswers = []
        self.negativeAnswers = []
        self.questionID = questionID



    def getID(self):
        return self.questionID

    def addPositive(self,pos):
    	self.positiveAnswers.append(pos)
    def addNegative(self,neg):
    	self.negativeAnswers.append(neg)


    def getPositiveAnswers(self):
        return self.positiveAnswers
    def getNegativeAnswers(self):
        return self.negativeAnswers

    def __str__(self):
        return self.sentence;
    def __repr__(self):
        return self.__str__()