from representations import Representations

class Answer:
    
    
    def __init__(self,questionID,answerID,answer,classification):
        self.answerID = answerID
        self.sentence = answer
        self.representation = Representations()
        self.classification = classification #classification of answer. >0 implies positive class
        self.questionID = questionID #id of question answer is related to.
        
    def __str__(self):
        return self.sentence;
    def __repr__(self):
        return self.__str__()
    
    def getID(self):
        return self.answerID

    def isPositive(self):
    	return self.classification > 0

