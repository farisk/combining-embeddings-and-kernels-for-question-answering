from sklearn import svm
from evaluation import Evaluation
import numpy as np

class AnswerExtractionSVM:


	def __init__(self,trainTestCombinations,costFactor):
		self.costFactor = costFactor
		self.trainTestCombinations = trainTestCombinations
	

	def useQAKernel(self,QAKernel):
		resultsForEachCostFactor = []
		for i in range(len(self.costFactor)):
			resultsForEachCostFactor.append([])

		for i in range(len(self.trainTestCombinations)): 
			print "=====================GROUP " + str(i+1) + "====================="
			((trainingQuestions,testingQuestions)) = self.trainTestCombinations[i];
			result = self.trainTestEvaluate(trainingQuestions,testingQuestions,QAKernel)
			
			for r in range(len(result)):
				resultsForEachCostFactor[r].append(result[r])

		

		for i in range(len(self.costFactor)):
			averagedResults = self.printAverageResults(resultsForEachCostFactor[i],self.costFactor[i])
			f1Measure = averagedResults[2]

	def printAverageResults(self,resultSet,cost):
		precisions = []
		recalls = []
		f1s = []
		for result in resultSet:
			precisions.append(result.computePrecision())
			recalls.append(result.computeRecall())
			f1s.append(result.computeF1Measure())

		averageP = np.mean(precisions)
		averageR = np.mean(recalls)
		averageF1 = np.mean(f1s)

		print "=========================AVERAGE" , cost ,"=========================="
		print "Recall:" , averageR , 'Precision:' , averageP ,"F1:" , averageF1 
		print str(cost) + "," + str(averageR)+ "," + str(averageP)+ "," + str(averageF1)

		return (averageP,averageR,averageF1)

	def trainTestEvaluate(self,trainingQuestions,testingQuestions,QAKernel):
		resultSet = [];
		#Put data into pairs
		(QAPairsTraining,yClassifications) = answersToQAPairs(trainingQuestions)
		(QAPairsTesting,actualClassifications) = answersToQAPairs(testingQuestions)
		#Computing support vector gram matrix
		gramMat = QAKernel.getTrainingGramMatrix(QAPairsTraining)

		#TEST
		predictGram = QAKernel.getTestingGramMatrix(QAPairsTraining,QAPairsTesting)

		for c in self.costFactor:
			clf = svm.SVC(kernel='precomputed',C=c)
			clf.fit(gramMat, yClassifications)

			predictedClasses = clf.predict(predictGram)
			
			#Evaluate
			print "=======evaluation: C=", c ,"========"
			results = Evaluation(actualClassifications,predictedClasses)
			results.printResults();
			resultSet.append(results)
		return resultSet


def saveNumpyMatrix(mat,outputfile):
	np.save(mat,outputFile)

def answersToQAPairs(questions):
    QAPairs = []
    classifications = []
    for q in questions:
    	for positiveAnswer in q.getPositiveAnswers():
    		QAPairs.append((q,positiveAnswer))
    		classifications.append(1)
    	
    	for negativeAnswer in q.getNegativeAnswers():
    		QAPairs.append((q,negativeAnswer))
    		classifications.append(0)
    return (QAPairs,classifications)