from question import Question
from answer import Answer
#load a file of questions formated as "id sentence", each new sentence is a new question.
#Return dictionary of Key(Question ID) -> Question
def loadQuestions(filePath):
    quetionFile = open(filePath,'r')  
    questionDict = {};
    for line in quetionFile:
        (questionSentence,questionID) = getIDAndSentence(line)
        questionDict[questionID] = Question(questionSentence,questionID)
    return questionDict

#split <num> "setence" into (num:int,sentence:string)
def getIDAndSentence(line):
    lineIntoWords = line.split()
    questionID = int(lineIntoWords[0])
    questionSentence = line.split(' ', 1)[1]
    questionSentence = questionSentence.replace("\n","")

    return (questionSentence,questionID)


def loadAnswersToQuestions(questionsDict,filePath):
    nextAnswerID = 0
    answerFile = open(filePath,'r')  
    answerList = []
    for line in answerFile:
        (answerSentence,questionID,classification) = getIDClassSentence(line)
        newAnswer = Answer(questionID,nextAnswerID,answerSentence,classification)
        nextAnswerID = nextAnswerID + 1
        answerList.append(newAnswer)

    
    populationQuestionsWithAnswers(questionsDict,answerList)#populate question positive and negative sample lists
    return answerList

#split <num> "setence" into (num:int,sentence:string)
def getIDClassSentence(line):
    lineIntoWords = line.split(' ',2)
    questionID = int(lineIntoWords[0])
    classification = int(lineIntoWords[1])
    questionSentence = lineIntoWords[2]
    questionSentence = questionSentence.replace("\n","")

    return (questionSentence,questionID,classification)
def populationQuestionsWithAnswers(questions,answers):
    for answer in answers:
        relatedQuestion = questions[answer.questionID]
        if answer.isPositive():
            relatedQuestion.addPositive(answer)
        else:
            relatedQuestion.addNegative(answer)

