from sklearn.feature_extraction.text import CountVectorizer
import representations
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np

from gensim.models import Word2Vec
from nltk.tokenize import RegexpTokenizer
from nltk.tree import *

#Load parse tree into syntactic representation of order answer list
def loadParseTrees(answers,parseTreeFile):
    counter = 0
    with open(parseTreeFile) as f:
        for line in f:
            newTree = Tree.fromstring(line)
            answers[counter].representation.setRepresentation(representations.SYNTACTICTREE,newTree)
            counter = counter + 1
            try:
                convToString = str(newTree)
            except UnicodeDecodeError:
                print "Unicode error when reading in parse tree."


def samplesToBagOfWordEmbeddingMax(samples):
    listOfSentences = samplesToListOfSentences(samples)
    listOfAveragedWordEmbeddings = []
    #Convert sentence into vector of word embeddings
    for sentence in listOfSentences:
        vectorOfEmbeddings = sentenceToWordEmbeddings(sentence)

        npMatrix = np.matrix(vectorOfEmbeddings)
        averagedEmbeddings = npMatrix.max(0).A1#average columns (each word embedding component)
        if len(vectorOfEmbeddings) == 0:
            averagedEmbeddings = np.zeros(100);
        listOfAveragedWordEmbeddings.append(averagedEmbeddings)
    updateSampleRepresentations(samples,listOfAveragedWordEmbeddings,representations.BOWEmbeddingsAveraged)


def sentenceToWordEmbeddings(sentence):
    vectorOfEmbeddings = []
    tokenizer = RegexpTokenizer(r'\w+')
    wordList = tokenizer.tokenize(sentence)
    for word in wordList:
        word = word.lower()
        if word in brownWordEmbeddings:
            wordEmbedding = getWordEmbedding(word)
            vectorOfEmbeddings.append(wordEmbedding)
    return vectorOfEmbeddings


brownWordEmbeddings = Word2Vec.load("data/brownEmbeddings.data")
def getWordEmbedding(word):
    return brownWordEmbeddings[word]


def samplesToBOW(samples):
    listOfSentences = samplesToListOfSentences(samples)
    listOfBOWVectors = BOW(listOfSentences)
    updateSampleRepresentations(samples,listOfBOWVectors,representations.BOW)
        

def BOW(listOfStrings):
    vectorizer = CountVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, max_features = 5000,lowercase=True,binary=True)
    bowFeatures = vectorizer.fit_transform(listOfStrings)
    
    return bowFeatures.toarray()

def samplesToListOfSentences(samples):
    #Create a list of strings from samples(either answer or questions)
    listOfSentences = []
    for s in samples:
        listOfSentences.append(s.sentence)

    return listOfSentences

def updateSampleRepresentations(samples,orderedfeatureVectors,representationName):
    for i in range(len(samples)):
        s = samples[i];
        newRepresentation = orderedfeatureVectors[i]
        sampleRepresentations = s.representation
        sampleRepresentations.setRepresentation(representationName,newRepresentation)