
def zeroDivide(top,bottom):
	if bottom == 0:
		return 0
	return top/bottom
class Evaluation:


	def __init__(self,actualClassifications,predictedClassifications):
		self.actualClassifications = actualClassifications
		self.predictedClassifications = predictedClassifications
	#hese quantities are also related to the (F_1) score, which is defined as the harmonic mean of precision and recall.
	def computeF1Measure(self):
		precision = self.computePrecision()
		recall = self.computeRecall()
		return 2 * ( zeroDivide( float(precision*recall),float(precision+recall)) )
	#Precision (P) is defined as the number of true positives (T_p) over the number of true positives plus the number of false positives (F_p).
	def computePrecision(self):
		truePositives = self.getTruePositives()
		falsePositives = self.getFalsePositives()

		precision = zeroDivide(float(truePositives) , float(truePositives+falsePositives))
		return (precision)

	#Recall (R) is defined as the number of true positives (T_p) over the number of true positives plus the number of false negatives (F_n).
	def computeRecall(self):
		truePositives = self.getTruePositives()
		falseNegatives = self.getFalseNegatives()
		recall = zeroDivide(float(truePositives),float(truePositives+falseNegatives))
		return recall

	def getTruePositives(self):
		truePositives = 0
		for i in range(len(self.actualClassifications)):
			actual = self.actualClassifications[i]
			predicted = self.predictedClassifications[i]
			if actual == 1 and predicted == 1:
				truePositives = truePositives + 1
		return truePositives

	def getFalsePositives(self):
		falsePositives = 0
		for i in range(len(self.actualClassifications)):
			actual = self.actualClassifications[i]
			predicted = self.predictedClassifications[i]
			if actual == 0 and predicted == 1:
				falsePositives = falsePositives + 1
		return falsePositives

	def getFalseNegatives(self):
		falseNegatives = 0
		for i in range(len(self.actualClassifications)):
			actual = self.actualClassifications[i]
			predicted = self.predictedClassifications[i]
			if actual == 1 and predicted == 0:
				falseNegatives = falseNegatives + 1
		return falseNegatives


	def printResults(self):
		print "Recall:" , str(self.computeRecall()) , 'Precision:' , self.computePrecision() ,"F1:" , str(self.computeF1Measure()) 
		print  "True Positives:", self.getTruePositives() , " False Positives:" , self.getFalsePositives() , " False Negatives:", self.getFalseNegatives()