from nltk.tree import *
from gensim.models import Word2Vec
import numpy as np
lambdaParam = 0.2
squareValueParam = 1 #1.5
def tkEmbedMod(tree1,tree2):
	sumC = 0;
	for node1 in tree1.subtrees():
		for node2 in tree2.subtrees():
			sumC = sumC + CWrapper(node1,node2,lambdaParam)

	clearResultStorage()
	return sumC


#storeComputations so they can be reused throughout
pastCComputations = {}
def CWrapper(node1,node2,lambdaParam):

	possibleKey1 = (str(node1),str(node2))

	if possibleKey1 in pastCComputations:
		return pastCComputations[possibleKey1]

	possibleKey2 = (str(node2),str(node1))
	if possibleKey2 in pastCComputations:
		return pastCComputations[possibleKey2]
	else:
		#Has not been computed before, so calculate result and store it
		computedC = C(node1,node2,lambdaParam)
		pastCComputations[possibleKey2] = computedC
		return computedC

def clearResultStorage():
	pastCComputations = {}

def C(node1, node2,lambdaParam):
	numberOfChildrenNode1 = getNumberOfChildren(node1)
	numberOfChildrenNode2 = getNumberOfChildren(node2)
	if not(numberOfChildrenNode1 == numberOfChildrenNode2):
		return 0;#Extra condition to quicken return when number of children are different
	elif not(areProductionsSame(node1,node2)):
		return 0;
	elif isNodePreTerminal(node1) and isNodePreTerminal(node2):
		return getSimScore(node1,node2) * lambdaParam
	else:
		mult = -1
		node1Children = getChildNodes(node1)
		node2Children = getChildNodes(node2)
		for i in range(numberOfChildrenNode1):
			recursiveResult = 1 + CWrapper(node1Children[i],node2Children[i],lambdaParam)
			if mult == -1:
				mult = recursiveResult;
			else:
				mult = mult * recursiveResult; 
		mult = mult * lambdaParam
		return max(0,mult)


def getNumberOfChildren(node):
	return len(node)
def getChildNodes(node):
	children = []
	for child in node:
		children.append(child)
	return children

def areProductionsSame(node1,node2):
	if node1.label() == node2.label():
		isNode1PreTerminal = isNodePreTerminal(node1)
		isNode2PreTerminal = isNodePreTerminal(node2)
		if isNode1PreTerminal or isNode2PreTerminal:#dont need to do these checks if check children b4 hand
			return (isNode1PreTerminal and isNode2PreTerminal);#First modification... return true even if words at leaves different
		else:
			#Check that both nodes have same productions
			productionsWithin =[]
			for production in node1:
				productionsWithin.append(production.label())
			for production in node2:
				if not(production.label() in productionsWithin):
					return False#Different production found so return false
			#Check that both nodes have same productions
			productionsWithin =[]
			for production in node2:
				productionsWithin.append(production.label())
			for production in node1:
				if not(production.label() in productionsWithin):
					return False#Different production found so return false
			return True;#productions are the same
	else:
		return False;#if rooted nodes label different then productions different

def isNodePreTerminal(node):
	return isTerminal(node[0])

def isTerminal(t):
	return isinstance(t, basestring)

def areLeavesSame(leaf1,leaf2):
	return leaf1 == leaf2;


#return a score of how similar two pre-terimnals leaves are
brownEmbeddings = Word2Vec.load("data/brownEmbeddings.data")
def getSimScoreCutOff(node1,node2):
	if (areLeavesSame(node1[0],node2[0])):
		return 1
	
	word1 = node1[0]
	word2 = node2[0]
	if word1 in brownEmbeddings and word2 in brownEmbeddings:
		if brownEmbeddings.similarity(word1,word2) > 0.8:
			return 1
	
	return 0

def getSimScore(node1,node2):
	simScoreRet = 0
	if (areLeavesSame(node1[0],node2[0])):
		return 1
	
	word1 = node1[0]
	word2 = node2[0]
	if word1 in brownEmbeddings and word2 in brownEmbeddings:
		simScore = brownEmbeddings.similarity(word1,word2)
		if simScore < 0:
			return 0
		return np.power(brownEmbeddings.similarity(word1,word2),squareValueParam)
	else:
		return 0

#jeffapple  = Tree.fromstring('(S ( NP (N Jeff) ) (VP (V ate) (NP (D the) (N apple))) )')
#jefffruit  = Tree.fromstring('(S ( NP (N Jeff) ) (VP (V ate) (NP (D the) (N fruit))) )')
#bobapple  = Tree.fromstring('(S ( NP (N Bob) ) (VP (V ate) (NP (D the) (N apple))) )')
#bobfruit  = Tree.fromstring('(S ( NP (N Bob) ) (VP (V ate) (NP (D the) (N fruit))) )')
#tree1=Tree.fromstring('(A ( B (D d) (E e) )  (C (F f ) (G g)))')
#tree2=Tree.fromstring('(A ( B (D d) (E e) )  (C (F kit) (G e)))')
#print tkEmbedMod(jeffapple,jefffruit)
