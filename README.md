Assignment for natural language processing class. Training an SVM for answer classification; to determine whether a given question-answer pair is valid or not ( the answer being correct for the question ). Both syntactic, and semantic analysis is used. Parse trees are combined with word embeddings for the most accurate results.

Dependencies:
Python 2.7
numpy
scikit ( pip install -U scikit-learn)
nltk (pip install -U nltk)
gensim (pip install --upgrade gensim)

Run:
1.Navigate into the code folder
2.execute: python main.py